package com.test.rappitest.view.search

import android.content.Context
import com.test.rappitest.adapters.SearchAdapter

class SearchPresenter(val view: Search.View) : Search.Presenter {
    private val model = SearchModel(this)

    override fun onError(msg: String) {
        view.onError(msg)
    }

    override fun fillRecycler(adapter: SearchAdapter) {
        view.fillRecycler(adapter)
    }

    override fun getSearch(context: Context, search: String) {
        model.getSearch(context, search)
    }
}