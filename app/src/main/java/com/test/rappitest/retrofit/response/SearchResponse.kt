package com.test.rappitest.retrofit.response

import com.google.gson.annotations.SerializedName
import com.test.rappitest.models.Search

data class SearchResponse(
    @SerializedName("results") val result: List<Search> = ArrayList()
)