package com.test.rappitest.database

import com.j256.ormlite.dao.RuntimeExceptionDao
import com.test.rappitest.models.MoviesTv

class MoviesTvDaoImpl {
    companion object {
        lateinit var dao: RuntimeExceptionDao<MoviesTv, Int>
    }

    init {
        dao = DBHelper.helper.getRuntimeExceptionDao(MoviesTv::class.java)
    }

    fun insert(dto: MoviesTv): Int {
        return dao.create(dto)
    }

    fun update(dto: MoviesTv): Int {
        return dao.update(dto)
    }

    fun getCount(type: Int, category: Int): Int {
        var count = 0
        var lst: List<MoviesTv> = ArrayList()
        val qB = MoviesTv.getRepository().queryBuilder()
        try {
            qB.where().eq("type", type).and().eq("category", category)
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (lst.isNotEmpty())
            count = lst.size
        return count
    }

    fun getCountVideo(): Int {
        var count = 0
        var lst: List<MoviesTv> = ArrayList()
        val qB = MoviesTv.getRepository().queryBuilder()
        try {
            qB.where().eq("hasVideo", true)
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (lst.isNotEmpty())
            count = lst.size
        return count
    }

    fun getMoviesTv(type: Int, category: Int): List<MoviesTv> {
        var lst: List<MoviesTv> = ArrayList()
        val qB = MoviesTv.getRepository().queryBuilder()
        try {
            qB.where().eq("type", type).and().eq("category", category)
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return lst
    }

    fun getMoviesTv(type: Int, category: Int, title: String): List<MoviesTv> {
        var lst: List<MoviesTv> = ArrayList()
        val qB = MoviesTv.getRepository().queryBuilder()
        try {
            qB.where().eq("type", type).and().eq("category", category).and().like("title", "%$title%")
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return lst
    }

    fun deleteMoviesTv(type: Int, category: Int) {
        try{
            dao.executeRawNoArgs("DELETE FROM movies_tv WHERE type=$type AND category=$category")
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
}