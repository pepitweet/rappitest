package com.test.rappitest.commons

object Const {
    const val TMDB_API_KEY = "787a5ef15cf103f7e4037a56ac233431"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/w500%s"

    const val API_KEY = "api_key"
    const val LANGUAGE = "language"
    const val PAGE = "page"
    const val REGION = "region"
    const val QUERY = "query"

    object Endpoints {
        private const val MOVIE = "movie"
        private const val SERIES = "tv"
        const val MOVIES_POPULAR = "$MOVIE/popular"
        const val MOVIES_TOP_RATED = "$MOVIE/top_rated"
        const val MOVIES_UPCOMING = "$MOVIE/upcoming"

        const val SERIES_POPULAR = "$SERIES/popular"
        const val SERIES_TOP_RATED = "$SERIES/top_rated"

        const val SEARCH = "search/multi"
    }

    enum class ServiceCode {
        UNKNOWN_SERVICE,
        GET_MOVIES_POPULAR,
        GET_MOVIES_TOP_RATE,
        GET_MOVIES_UPCOMING,
        GET_SERIES_POPULAR,
        GET_SERIES_TOP_RATE,
        GET_SERIES_UPCOMING,
        GET_SEARCH
    }
}