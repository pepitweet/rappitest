package com.test.rappitest.view.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.test.rappitest.database.MoviesTvDaoImpl
import com.test.rappitest.models.MoviesTv
import com.test.rappitest.retrofit.RetrofitHelper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

const val MOVIES = 0
const val SERIES = 1

const val POPULAR = 0
const val TOP_RATED = 1
const val UPCOMING = 2
class PageViewModel : ViewModel() {

    private val typeIndex = MutableLiveData<Int>()
    private val catIndex = MutableLiveData<Int>()
    private val items = MutableLiveData<List<MoviesTv>>()
    private val searchTitle = MutableLiveData<String>()
    private val toastMesssage = MutableLiveData<String>()

    val itemsAdapter: LiveData<List<MoviesTv>> = Transformations.map(items) {
        if (it.isEmpty()) toastMesssage.postValue("No hay información por mostrar")
        it
    }

    val text: LiveData<String> = Transformations.map(typeIndex) {
        getInfo(it, catIndex.value)
        "Hello world from section: $it catIndex ${catIndex.value}"
    }

    val textIdx: LiveData<String> = Transformations.map(catIndex) {
        getInfo(typeIndex.value, it)
        "Cat $it Type: ${typeIndex.value}"
    }

    val textToast: LiveData<String> = Transformations.map(toastMesssage) {
        it
    }

    fun setIndex(index: Int) {
        typeIndex.value = index
    }

    fun setCatIndex(index: Int) {
        catIndex.value = index
    }

    fun searchData(title: String) {
        var tp = typeIndex.value ?: 0
        if (tp > 0)
            tp--
        val cat = catIndex.value ?: 0
        searchTitle.value = title
        searchMovieTv(tp, cat, title)
    }

    fun searchMovieTv(type: Int, category: Int, title: String) {
        items.value = MoviesTvDaoImpl().getMoviesTv(type, category, title)
    }

    fun getInfo(type: Int?, category: Int?) {
        var tp = type ?: 0
        if (tp > 0)
            tp--
        val cat = category ?: 0
        when (tp) {
            MOVIES -> {
                getMovies(cat)
            }
            SERIES -> {
                getSeries(cat)
            }
        }
    }

    fun getMovies(category: Int) {
        val moviesTv = MoviesTvDaoImpl().getMoviesTv(MOVIES, category)

        if (moviesTv.isNotEmpty()) {
            items.value = moviesTv
            return
        }

        GlobalScope.launch {
            val response = when (category) {
                POPULAR ->  RetrofitHelper.getMoviesPopular()
                TOP_RATED -> RetrofitHelper.getMoviesTopRated()
                else -> RetrofitHelper.getMoviesUpcoming()
            }

            response.objeto?.let {
                MoviesTvDaoImpl().deleteMoviesTv(MOVIES, category)

                for (item in it.result) {
                    val movie = MoviesTv(
                        item.id,
                        MOVIES,
                        category,
                        item.title,
                        item.voteAverage.toFloat(),
                        item.overview,
                        item.posterPath,
                        item.backdropPath,
                        item.video
                    )
                    MoviesTvDaoImpl().insert(movie)
                }
            }
            items.postValue(MoviesTvDaoImpl().getMoviesTv(MOVIES, category))
        }
    }

    fun getSeries(category: Int) {
        if (category == UPCOMING) {
            toastMesssage.postValue("No hay información por mostrar")
            items.postValue(ArrayList())
            return
        }
        val moviesTv = MoviesTvDaoImpl().getMoviesTv(SERIES, category)

        if (moviesTv.isNotEmpty()) {
            items.value = moviesTv
            return
        }
        GlobalScope.launch {
            val response = when (category) {
                POPULAR ->  RetrofitHelper.getSeriesPopular()
                else -> RetrofitHelper.getSeriesTopRated()
            }

            response.objeto?.let {
                MoviesTvDaoImpl().deleteMoviesTv(SERIES, category)

                for (item in it.results) {
                    val serie = MoviesTv(
                        item.id,
                        SERIES,
                        category,
                        item.name,
                        item.voteAverage.toFloat(),
                        item.overview,
                        item.posterPath,
                        item.backdropPath
                    )
                    MoviesTvDaoImpl().insert(serie)
                }
            }
            items.postValue(MoviesTvDaoImpl().getMoviesTv(SERIES, category))
        }
    }
}