package com.test.rappitest.models

import com.google.gson.annotations.SerializedName

data class Search(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String = "",
    @SerializedName("title") val title: String = "",
    @SerializedName("media_type") val mediaType: String,
    @SerializedName("vote_average") val rate: Double,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("overview") val overview: String
)