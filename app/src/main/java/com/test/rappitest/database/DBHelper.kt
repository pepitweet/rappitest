package com.test.rappitest.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import com.test.rappitest.models.MainAplication
import com.test.rappitest.models.MoviesTv

private const val DATABASE_VERSION = 1
class DBHelper(context: Context, path: String) : OrmLiteSqliteOpenHelper(context, path, null, DATABASE_VERSION) {
    companion object {
        private var mInstance: DBHelper?= null
        val helper: DBHelper
            get() {
                if(mInstance == null)
                    mInstance = DBHelper(MainAplication.appContext!!, "${MainAplication.appContext!!.filesDir.path}/movies_tv.db")

                return mInstance!!
            }
    }

    class DaoGet<T>{
        companion object {
            operator fun <T> get(clazz: Class<T>) : RuntimeExceptionDao<T, Int> {
                return DBHelper.helper.getRuntimeExceptionDao<RuntimeExceptionDao<T, Int>, T>(clazz)
            }
        }
    }

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource, MoviesTv::class.java)
    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        onCreate(database, connectionSource)
    }
}