package com.test.rappitest.models

import com.google.gson.annotations.SerializedName

data class Serie(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("overview") val overview: String,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("backdrop_path") val backdropPath: String?
)