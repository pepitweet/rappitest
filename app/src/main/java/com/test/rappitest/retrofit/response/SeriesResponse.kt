package com.test.rappitest.retrofit.response

import com.google.gson.annotations.SerializedName
import com.test.rappitest.models.Serie

data class SeriesResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val results: List<Serie> = ArrayList()
)