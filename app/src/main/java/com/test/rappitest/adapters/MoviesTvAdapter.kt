package com.test.rappitest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.rappitest.R
import com.test.rappitest.commons.Const
import com.test.rappitest.models.MoviesTv
import com.test.rappitest.view.MainView
import kotlinx.android.synthetic.main.custom_movie_item.view.*


class MoviesTvAdapter (val adapterContex: Context, val items: List<MoviesTv>) : RecyclerView.Adapter<MoviesTvAdapter.MoviesTvViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): MoviesTvViewHolder {
        return MoviesTvViewHolder(LayoutInflater.from(adapterContex).inflate(R.layout.custom_movie_item, viewGroup, false))
    }

    override fun onBindViewHolder(holder: MoviesTvViewHolder, position: Int) {
        val movieTv = items[position]
        holder.tvTitle.text = movieTv.title
        holder.tvOverview.text = movieTv.overview
        holder.rbRating.rating = movieTv.rate.div(2)

        if (movieTv.posterPath != null) {
            println(String.format(Const.IMAGE_URL, movieTv.posterPath))
            Picasso.get()
                .load(String.format(Const.IMAGE_URL, movieTv.posterPath))
                .into(holder.ivPoster)
        }

        holder.cvContainer.setOnClickListener {
            if (adapterContex is MainView) {
                val activity = adapterContex
                activity.showDetails(movieTv)
            }
        }
    }

    class MoviesTvViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivPoster = view.ivPoster
        val tvTitle = view.tvTitle
        val rbRating = view.rbRating
        val tvOverview = view.tvOverview
        val cvContainer = view.cvContainer
    }
}