package com.test.rappitest

import com.test.rappitest.retrofit.RetrofitHelper
import kotlinx.coroutines.runBlocking
import org.junit.Assert.fail
import org.junit.Test
import java.net.HttpURLConnection

private const val STATUS_CODE = "Status code: %s"
private const val QUERY_SEARCH = "FEL"

class UnitTest {
    @Test
    fun downloadPopular() {
        println("PRUEBA MOVIES POPULAR")
        runBlocking {
            val response = RetrofitHelper.getMoviesPopular()

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (movie in response.objeto!!.result) {
                    println("Pelicula: ${movie.title} | Rate: ${movie.voteAverage} | Votes: ${movie.voteCount}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }

    @Test
    fun downloadTopRated() {
        println("PRUEBA MOVIES TOP RATED")
        runBlocking {
            val response = RetrofitHelper.getMoviesTopRated()

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (movie in response.objeto!!.result) {
                    println("Pelicula: ${movie.title} | Rate: ${movie.voteAverage} | Votes: ${movie.voteCount}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }

    @Test
    fun downloadUpcoming() {
        println("PRUEBA MOVIES UPCOMING")
        runBlocking {
            val response = RetrofitHelper.getMoviesUpcoming()

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (movie in response.objeto!!.result) {
                    println("Pelicula: ${movie.title} | Rate: ${movie.voteAverage} | Votes: ${movie.voteCount}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }

    @Test
    fun downloadSeriesPopular() {
        println("PRUEBA SERIES POPULAR")
        runBlocking {
            val response = RetrofitHelper.getSeriesPopular()

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (serie in response.objeto!!.results) {
                    println("Pelicula: ${serie.name} | Rate: ${serie.voteAverage}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }

    @Test
    fun downloadSeriesTopRated() {
        println("PRUEBA SERIES TOP RATED")
        runBlocking {
            val response = RetrofitHelper.getSeriesTopRated()

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (serie in response.objeto!!.results) {
                    println("Pelicula: ${serie.name} | Rate: ${serie.voteAverage}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }

    @Test
    fun search() {
        println("PRUEBA SEARCH")
        runBlocking {
            val response = RetrofitHelper.getSearch(QUERY_SEARCH)

            if (response.statusCode == HttpURLConnection.HTTP_OK) {
                response.objeto ?: fail("La respuesta no contiene datos")

                for (result in response.objeto!!.result) {
                    println("Nombre: ${if(result.title.isNullOrBlank()) result.name else result.title} | Rate: ${result.rate} | Tipo: ${result.mediaType}")
                }
                assert(true)
            } else
                fail(String.format(STATUS_CODE, response.statusCode.toString()))
        }
    }
}