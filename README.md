# Rappi Test

Es una aplicación que responde a la prueba practica enviada, está creado
para Android de forma nativa en Kotlin, usando Android Studio 3.4

### Persistencia de datos

Para la capa de persistencia de datos se utilizó ORMLite para un manejo
de una base de datos SQLite, dentro del paquete **database** se encuentra
la clase DBHelper, la cual es la encargada de manipular la base de datos;
así como la clase de Acceso a Datos de la tabla utlizada.

### Consumo de API Rest

Para el consumo de la API Rest se utlizó Retrofit como herramienta de apoyo
dentro del paquete **retrofit** encontraremos la interfaz ApiClient, que se 
encarga de realizar un mapeo de los endpoints utlizados así como de los 
parámetros que utlizan para realizar las consultas.

Se hace uso de la clase RetrofitHelper para la consulta de los mismos de
forma asincrona apoyado por las corutinas que nos ofrece Kotlin.

### Paquetes

#### adapters

Aquí se ubican las clases que se usan como adaptadores.

* **Movies Adapter** y **SearchAdapter** se encargan de llenar los recyclersviews con la información local y online
* **SectionsPageAdapter** se encarga de llenar las secciones de las Tabs

#### commons

Clases que se usan de forma común por todo el proyecto
 
* **Const** es una clase tipo Object que contiene las constantes que son utilidas en el proyecto
 
#### database

Clases utilizadas para la persistencia de datos

* **DBHelper** se encarga de la manipulación del archivo de base datos
* **MoviesTvDaoImpl** se encarga de los CRUD a la tabla MoviesTv

#### models

* **MainAplication** es la clase de Aplicación que 
* **Movie** Modelo que mapea las peliculas obtenidas desde API Rest
* **MoviesTv** Es el modelo de la tabla donde se guardan los datos localmente
* **Search** Modelo que mapea los resultados de las busquedas realizadas desde el API Rest
* **Serie** Modelo que mapea las series obtenidas del API Rest

#### retrofit

* **ApiClient** Interfaz para el consumo de cada endpoint
* **RetrofitHelper** Se encarga de procesar las peticiones al API Rest

Responses son los Pojos para mapear las respuestas JSON

#### view

Las actividades y fragmentos que tienen interacción directa con el usuario


----

>En qué consiste el principio de responsabilidad única? Cuál es su propósito?

Un objeto debe realizar una única cosa. El proposito es que cada objeto este destinado solo a un objetivo para así proteger el código de los cambios, ya que de esta forma es poco probable modificar un funcionamiento que no deseamos

>Qué características tiene, según su opinión, un “buen” código o código limpio?

Un código limpio siempre está organizado, teniendo una correcta identación, nombres de variables/métodos acordes a su función. Un buen código es fácil de leer a primera vista.

