package com.test.rappitest.retrofit

import com.test.rappitest.commons.Const
import com.test.rappitest.commons.Const.API_KEY
import com.test.rappitest.commons.Const.Endpoints.MOVIES_POPULAR
import com.test.rappitest.commons.Const.Endpoints.MOVIES_TOP_RATED
import com.test.rappitest.commons.Const.Endpoints.MOVIES_UPCOMING
import com.test.rappitest.commons.Const.Endpoints.SEARCH
import com.test.rappitest.commons.Const.Endpoints.SERIES_POPULAR
import com.test.rappitest.commons.Const.Endpoints.SERIES_TOP_RATED
import com.test.rappitest.commons.Const.LANGUAGE
import com.test.rappitest.commons.Const.PAGE
import com.test.rappitest.commons.Const.QUERY
import com.test.rappitest.commons.Const.REGION
import com.test.rappitest.retrofit.response.MoviesResponse
import com.test.rappitest.retrofit.response.SearchResponse
import com.test.rappitest.retrofit.response.SeriesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiClient {

    @ServiceCode(Const.ServiceCode.GET_MOVIES_POPULAR)
    @GET(MOVIES_POPULAR)
    fun getMoviesPopular(
        @Query(API_KEY) apiKey: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null,
        @Query(REGION) region: String? = null
    ): Call<MoviesResponse>

    @ServiceCode(Const.ServiceCode.GET_MOVIES_TOP_RATE)
    @GET(MOVIES_TOP_RATED)
    fun getMoviesTopRated(
        @Query(API_KEY) apiKey: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null,
        @Query(REGION) region: String? = null
    ): Call<MoviesResponse>

    @ServiceCode(Const.ServiceCode.GET_MOVIES_UPCOMING)
    @GET(MOVIES_UPCOMING)
    fun getMoviesUpcoming(
        @Query(API_KEY) apiKey: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null,
        @Query(REGION) region: String? = null
    ): Call<MoviesResponse>

    @ServiceCode(Const.ServiceCode.GET_SERIES_POPULAR)
    @GET(SERIES_POPULAR)
    fun getSeriesPopular(
        @Query(API_KEY) apiKey: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null
    ): Call<SeriesResponse>

    @ServiceCode(Const.ServiceCode.GET_SERIES_TOP_RATE)
    @GET(SERIES_TOP_RATED)
    fun getSeriesTopRated(
        @Query(API_KEY) apiKey: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null
    ): Call<SeriesResponse>

    @ServiceCode(Const.ServiceCode.GET_SEARCH)
    @GET(SEARCH)
    fun getSearch(
        @Query(API_KEY) apiKey: String,
        @Query(QUERY) query: String,
        @Query(LANGUAGE) language: String? = null,
        @Query(PAGE) page: Int? = null
    ): Call<SearchResponse>
}