package com.test.rappitest.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paulrybitskyi.persistentsearchview.utils.VoiceRecognitionDelegate
import com.test.rappitest.R
import com.test.rappitest.adapters.SearchAdapter
import com.test.rappitest.view.search.Search
import com.test.rappitest.view.search.SearchPresenter
import kotlinx.android.synthetic.main.activity_search.*


class SearchView : AppCompatActivity(), Search.View {
    private var presenter: Search.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        title = "The Movie DB"

        presenter = SearchPresenter(this)

        with(persistentSearchView) {
            setQueryInputHint("Buscar Pelicula/Serie/Actor")
            setOnLeftBtnClickListener {
                finish()
            }

            setVoiceRecognitionDelegate(VoiceRecognitionDelegate(this@SearchView))

            setOnSearchConfirmedListener { searchView, query ->
                searchView.collapse()
                presenter?.getSearch(this@SearchView, query)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        VoiceRecognitionDelegate.handleResult(persistentSearchView, requestCode, resultCode, data)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
    }

    override fun onError(msg: String) {
        runOnUiThread {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
    }

    override fun fillRecycler(adapter: SearchAdapter) {
        runOnUiThread {
            reciclador.layoutManager = LinearLayoutManager(this@SearchView, RecyclerView.VERTICAL, false)
            reciclador.adapter = adapter
        }
    }
}