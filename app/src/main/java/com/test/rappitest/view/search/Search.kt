package com.test.rappitest.view.search

import android.content.Context
import com.test.rappitest.adapters.SearchAdapter

interface Search {
    interface View {
        fun onError(msg: String)
        fun fillRecycler(adapter: SearchAdapter)
    }

    interface Presenter {
        fun onError(msg: String)
        fun fillRecycler(adapter: SearchAdapter)

        fun getSearch(context: Context, search: String)
    }

    interface Model {
        fun getSearch(context: Context, search: String)
    }
}