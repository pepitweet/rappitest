package com.test.rappitest.retrofit.response

import com.test.rappitest.commons.Const

class RequestResponse<T>(val serviceCode: Const.ServiceCode) {
    var objeto: T? = null
    var statusCode: Int = 0
    var throeable: Throwable? = null
}