package com.test.rappitest.retrofit

import com.test.rappitest.commons.Const

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class ServiceCode (val serviceCode: Const.ServiceCode)