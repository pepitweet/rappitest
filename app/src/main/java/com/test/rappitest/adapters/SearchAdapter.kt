package com.test.rappitest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.rappitest.R
import com.test.rappitest.commons.Const
import com.test.rappitest.models.Search
import kotlinx.android.synthetic.main.custom_search_item.view.*

class SearchAdapter (private val adapterContext: Context, private val items: List<Search>)  : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder(LayoutInflater.from(adapterContext).inflate(R.layout.custom_search_item, parent, false))
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val searchItem = items[position]
        holder.tvTitle.text = if (searchItem.title.isNullOrBlank()) searchItem.name else searchItem.title
        holder.tvOverview.text = searchItem.overview
        holder.rbRating.rating = searchItem.rate.toFloat().div(2)
        holder.tvType.text = searchItem.mediaType

        if (searchItem.posterPath != null) {
            println(String.format(Const.IMAGE_URL, searchItem.posterPath))
            Picasso.get()
                .load(String.format(Const.IMAGE_URL, searchItem.posterPath))
                .into(holder.ivPoster)
        }

        holder.cvContainer.setOnClickListener {

        }
    }

    class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivPoster = view.ivPoster
        val tvTitle = view.tvTitle
        val rbRating = view.rbRating
        val tvOverview = view.tvOverview
        val cvContainer = view.cvContainer
        val tvType = view.tvType
    }
}