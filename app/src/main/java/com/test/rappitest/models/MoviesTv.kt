package com.test.rappitest.models

import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.test.rappitest.database.DBHelper

@DatabaseTable(tableName = "movies_tv")
data class MoviesTv(
    @DatabaseField(columnName = "id_movie_tv") var idMovieTv: Int,
    @DatabaseField var type: Int,
    @DatabaseField var category: Int,
    @DatabaseField var title: String,
    @DatabaseField var rate: Float,
    @DatabaseField var overview: String,
    @DatabaseField var posterPath: String?,
    @DatabaseField var backdropPath: String?,
    @DatabaseField var hasVideo: Boolean = false
) {
    constructor() : this (-1, -1, -1, "", 0f, "", null, null)

    companion object {
        fun getRepository(): RuntimeExceptionDao<MoviesTv, Int> {
            return DBHelper.DaoGet[MoviesTv::class.java]
        }
    }
}