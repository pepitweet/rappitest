package com.test.rappitest.view.search

import android.content.Context
import com.test.rappitest.adapters.SearchAdapter
import com.test.rappitest.retrofit.RetrofitHelper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SearchModel(val presenter: Search.Presenter) : Search.Model {
    private var items: List<com.test.rappitest.models.Search> = ArrayList()
    override fun getSearch(context: Context, search: String) {
        GlobalScope.launch {
            val response = RetrofitHelper.getSearch(search)
            if (response.objeto != null)
                items = response.objeto!!.result
             else
                items = ArrayList()

            if (items.isNullOrEmpty())
                presenter.onError("No se encontraron resultados")

            val adapter = SearchAdapter(context, items)
            presenter.fillRecycler(adapter)
        }
    }
}