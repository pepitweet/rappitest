package com.test.rappitest.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.squareup.picasso.Picasso
import com.test.rappitest.R
import com.test.rappitest.adapters.SectionsPagerAdapter
import com.test.rappitest.commons.Const
import com.test.rappitest.models.MoviesTv
import kotlinx.android.synthetic.main.activity_main_view.*
import kotlinx.android.synthetic.main.dialog_show_details.view.*

class MainView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_view)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        view_pager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(view_pager)

        setSupportActionBar(toolbar)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    fun showDetails(movieTv: MoviesTv){
        val dialog = AlertDialog.Builder(this)
        val view = layoutInflater.inflate(R.layout.dialog_show_details, null)

        view.tvTitle.text = movieTv.title
        view.ratingBar.rating = movieTv.rate.div(2)
        view.tvOverview.text = movieTv.overview

        if (movieTv.posterPath != null) {
            Picasso.get().load(String.format(Const.IMAGE_URL, movieTv.backdropPath)).into(view.ivPoster)
        }

        dialog.setView(view)
        val alert = dialog.create()
        alert?.window?.attributes?.windowAnimations = R.style.DialogAnimation
        alert.show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item ?: return false
        when (item.itemId) {
            R.id.search -> {
                startActivity(Intent(this, SearchView::class.java))
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}