package com.test.rappitest.retrofit

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.test.rappitest.commons.Const
import com.test.rappitest.commons.Const.BASE_URL
import com.test.rappitest.commons.Const.TMDB_API_KEY
import com.test.rappitest.retrofit.response.MoviesResponse
import com.test.rappitest.retrofit.response.RequestResponse
import com.test.rappitest.retrofit.response.SearchResponse
import com.test.rappitest.retrofit.response.SeriesResponse
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private const val CONNECTION_TIMEOUT: Long = 1000
private const val DATA_RETRIEVAL_TIMEOUT:Long = 30000
object RetrofitHelper {
    private val builder = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(
            OkHttpClient
                .Builder()
                .callTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(DATA_RETRIEVAL_TIMEOUT, TimeUnit.MILLISECONDS)
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create(builder))
        .build()

    private var restClient = retrofit.create(ApiClient::class.java)

    suspend fun getMoviesPopular(): RequestResponse<MoviesResponse> {
        val call = restClient.getMoviesPopular(TMDB_API_KEY, null, null, null)
        return performRequest(call)
    }

    suspend fun getMoviesTopRated() : RequestResponse<MoviesResponse> {
        val call = restClient.getMoviesTopRated(TMDB_API_KEY, null, null, null)
        return performRequest(call)
    }

    suspend fun getMoviesUpcoming() : RequestResponse<MoviesResponse> {
        val call = restClient.getMoviesUpcoming(TMDB_API_KEY, null, null, null)
        return performRequest(call)
    }

    suspend fun getSeriesPopular() : RequestResponse<SeriesResponse> {
        val call = restClient.getSeriesPopular(TMDB_API_KEY, null, null)
        return performRequest(call)
    }

    suspend fun getSeriesTopRated() : RequestResponse<SeriesResponse> {
        val call = restClient.getSeriesTopRated(TMDB_API_KEY, null, null)
        return performRequest(call)
    }

    suspend fun getSearch(query: String) : RequestResponse<SearchResponse> {
        val call = restClient.getSearch(TMDB_API_KEY, query, null, null)
        return performRequest(call)
    }

    private suspend fun <T>performRequest(call: Call<T>) = suspendCoroutine<RequestResponse<T>> { continuation ->
        val serviceCode = getServiceCode(call.request())
        val result = RequestResponse<T>(serviceCode)

        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                result.statusCode = response.code()
                result.objeto = response.body()

                continuation.resume(result)
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                result.throeable = t
                continuation.resume(result)
            }
        })
    }

    fun getServiceCode(request: Request): Const.ServiceCode {
        val invocation = request.tag(Invocation::class.java)
        val myAnnotation = invocation?.method()?.getAnnotation(ServiceCode::class.java)
        return myAnnotation?.serviceCode ?: Const.ServiceCode.UNKNOWN_SERVICE
    }
}